import re


def delete_multi_line_comment(data):
    data = re.sub(re.compile("'''.*'''", re.DOTALL), "", data)
    return re.sub(re.compile('""".*"""', re.DOTALL), "", data)


def delete_one_line_comment(data):
    return re.sub(re.compile("#.*"), "", data)


def delete_strings(data):
    data = re.sub(re.compile("'.*'"), "", data)
    return re.sub(re.compile('".*"'), "", data)


def delete_numbers(data):
    return re.sub("\d+", "", data)
