import clear_file as c


def test_delete_multi_line_comment():
    assert c.delete_multi_line_comment('"""aaaaaaa"""') == ""
    assert c.delete_multi_line_comment('"""aaaaaaa"""aaaa') == "aaaa"
    assert c.delete_multi_line_comment('"""\naaaaaaa\n"""aaaa') == "aaaa"
    assert c.delete_multi_line_comment('aaaa"""aaaaaaa"""') == "aaaa"
    assert c.delete_multi_line_comment('aa"""aaaaaaa"""aa') == "aaaa"
    assert c.delete_multi_line_comment("'''aaaaaaa'''") == ""
    assert c.delete_multi_line_comment("aa'''aaaaaaa'''aa") == "aaaa"


def test_delete_one_line_comment():
    assert c.delete_one_line_comment('#aaaaaa') == ""
    assert c.delete_one_line_comment('aaaa#aaaaaa') == "aaaa"
    assert c.delete_one_line_comment('#aaaaaa\naaaa') == "\naaaa"
    assert c.delete_one_line_comment('aa#aaaaaa\naa') == "aa\naa"


def test_delete_strings():
    assert c.delete_strings('"aaaaa"') == ""
    assert c.delete_strings("'aaaaa'") == ""
    assert c.delete_strings('aaaa"aaaaa"') == "aaaa"
    assert c.delete_strings('aa"aaaaa"aa') == "aaaa"
    assert c.delete_strings('aa"xxxx"bb\'xxx\'cc') == "aabbcc"


def test_delete_numbers():
    assert c.delete_numbers("3") == ""
    assert c.delete_numbers("51231 3") == " "
    assert c.delete_numbers("5a2313") == "a"
    assert c.delete_numbers("5aaaa5") == "aaaa"